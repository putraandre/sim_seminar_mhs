<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <style type="text/css">
        .wrapper{
            width: 1000px;
            margin: 0 auto;
        }
        .page-header h2{
            margin-top: 0;
        }
        table tr td:last-child a{
            margin-right: 15px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</head>
<body>
  <div class="wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="page-header clearfix">
              <h2 class="pull-left">System Informasi Jadwal Seminar Skripsi Mahasiswa</h2>
              <a href="input.php" class="btn btn-success pull-right">+ Tambah Baru</a>
          </div>
            <?php
            // Include config file
            require_once "config.php";
            require_once "format_tgl.php";
            $sql = "SELECT * FROM mahasiswa";
            if($result = mysqli_query($db, $sql)){
                if(mysqli_num_rows($result) > 0){
                    echo "<table class='table table-bordered table-striped'>";
                        echo "<thead>";
                            echo "<tr>";
                                echo "<th>Nim</th>";
                                echo "<th>Nama</th>";
                                echo "<th>Judul</th>";
                                echo "<th>Dosen Pembimbing</th>";
                                echo "<th>Tanggal</th>";
                                echo "<th>Jam</th>";
                                echo "<th>Ruangan</th>";
                                echo "<th>Dosen Penguji</th>";
                                echo "<th>Action</th>";
                            echo "</tr>";
                        echo "</thead>";
                        echo "<tbody>";
                        while($row = mysqli_fetch_array($result)){
                            echo "<tr>";
                                echo "<td>" . $row['nim'] . "</td>";
                                echo "<td>" . $row['nama'] . "</td>";
                                echo "<td>" . $row['judul'] . "</td>";
                                echo "<td>" . $row['dosenPembimbing'] . "</td>";
                                echo "<td>" . tanggalIndo($row['tanggal']) . "</td>";
                                echo "<td>" . $row['jam'] . "</td>";
                                echo "<td>" . $row['ruangan'] . "</td>";
                                echo "<td>" . $row['dosenPenguji'] . "</td>";
                                echo "<td>";
                                    echo "<a href='edit.php?nim=". $row['nim'] ."' title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
                                    echo "<a href='hapus.php?nim=". $row['nim'] ."' title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                                echo "</td>";
                            echo "</tr>";
                        }
                        echo "</tbody>";
                    echo "</table>";
                    // Free result set
                    mysqli_free_result($result);
                } else{
                    echo "<p class='lead'><em>No records were found.</em></p>";
                }
            } else{
                echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
            }

            // Close connection
            mysqli_close($link);
            ?>
        </div>
      </div>
    </div>
  </div>
</body>
</html>