<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>input</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
  <style type="text/css">
    .wrapper{
      width: 500px;
      margin: 0 auto;
    }
  </style>
</head>
<body>
  <div class="wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="page-header">
            <h2>Tambah Data</h2>
          </div>
          <p>Silahkan isi form di bawah ini kemudian submit untuk menambahkan data mahasiswa yang akan seminar skripsi.</p>
          <form action="proses_daftar.php" method="post">
            <div class="form-group">
              <label>Nim</label>
              <input type="text" name="nim" class="form-control">
            </div>

            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" class="form-control" value="">
            </div>

            <div class="form-group">
              <label>Judul</label>
              <textarea name="judul" class="form-control"></textarea>
            </div>

            <div class="form-group">
              <label>Dosen Pembimbing</label>
              <input type="text" name="dosenPembimbing" class="form-control">
            </div>

            <div class="form-group">
              <label>Tanggal</label>
              <input type="date" name="tanggal" class="form-control">
            </div>

            <div class="form-group">
              <label>Jam</label>
              <input type="text" name="jam" class="form-control">
            </div>

            <div class="form-group">
              <label>Ruangan</label>
              <input type="text" name="ruangan" class="form-control">
            </div>

            <div class="form-group">
              <label>Dosen Penguji</label>
              <input type="text" name="dosenPenguji" class="form-control">
            </div>
            <input type="submit" class="btn btn-primary" value="Submit">
          </form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>