<?php

  require_once "config.php";

  if(isset($_POST['update']))
  {   
    $nim = $_POST['nim'];
    $nama = $_POST['nama'];
    $judul = $_POST['judul'];
    $dosenPembimbing = $_POST['dosenPembimbing'];
    $tanggal = $_POST['tanggal'];
    $jam = $_POST['jam'];
    $ruangan = $_POST['ruangan'];
    $dosenPenguji = $_POST['dosenPenguji'];

    $sql = mysqli_query($db, "UPDATE mahasiswa SET nim='$nim',nama='$nama',judul='$judul',dosenPembimbing='$dosenPembimbing',tanggal='$tanggal',jam='$jam',ruangan='$ruangan',dosenPenguji='$dosenPenguji' WHERE nim=$nim");
    $result = mysqli_query($db, $sql);
    
    if( $result ) {
    // kalau berhasil alihkan ke halaman index.php dengan status=sukses
    header('Location: index.php?status=sukses');
  } else {
    // kalau gagal alihkan ke halaman indek.php dengan status=gagal
    header('Location: index.php?status=gagal');
  }
  }
?>

<?php
  $nim = $_GET['nim'];
  $result = mysqli_query($db, "SELECT * FROM mahasiswa WHERE nim=$nim");

  $row = mysqli_fetch_array($result);
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>edit</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
  <style type="text/css">
    .wrapper{
      width: 500px;
      margin: 0 auto;
    }
  </style>
</head>
<body>
  <div class="wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="page-header">
            <h2>Tambah Data</h2>
          </div>
          <p>Silahkan isi form di bawah ini kemudian submit untuk menambahkan data mahasiswa yang akan seminar skripsi.</p>
          <form action="edit.php" method="post">
            <div class="form-group">
              <label>Nim</label>
              <input type="text" name="nim" class="form-control" value="<?php echo $row['nim']?>">
            </div>

            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" class="form-control" value="<?php echo $row['nama'] ?>">
            </div>

            <div class="form-group">
              <label>Judul</label>
              <textarea name="judul" class="form-control"><?php echo $row['judul']?></textarea>
            </div>

            <div class="form-group">
              <label>Dosen Pembimbing</label>
              <input type="text" name="dosenPembimbing" class="form-control" value="<?php echo $row['dosenPembimbing'] ?>">
            </div>

            <div class="form-group">
              <label>Tanggal</label>
              <input type="date" name="tanggal" class="form-control" value="<?php echo $row['tanggal'] ?>">
            </div>

            <div class="form-group">
              <label>Jam</label>
              <input type="text" name="jam" class="form-control" value="<?php echo $row['jam'] ?>">
            </div>

            <div class="form-group">
              <label>Ruangan</label>
              <input type="text" name="ruangan" class="form-control" value="<?php echo $row['ruangan'] ?>">
            </div>

            <div class="form-group">
              <label>Dosen Penguji</label>
              <input type="text" name="dosenPenguji" class="form-control" value="<?php echo $row['dosenPenguji'] ?>">
            </div>
            <input type="submit" class="btn btn-primary" value="Submit" name="update">
          </form>
        </div>
      </div>
    </div>
  </div>
</body>
</html>